declare namespace HomeCilabModuleScssNamespace {
  export interface IHomeCilabModuleScss {
    about: string;
    button: string;
    content: string;
    desc: string;
    grid: string;
    info: string;
    lab: string;
    root: string;
  }
}

declare const HomeCilabModuleScssModule: HomeCilabModuleScssNamespace.IHomeCilabModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCilabModuleScssNamespace.IHomeCilabModuleScss;
};

export = HomeCilabModuleScssModule;
