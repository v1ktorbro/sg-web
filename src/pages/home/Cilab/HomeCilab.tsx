import { DATA_CILAB } from "../../../assets/data/cilab";
import { SectionsIdsEnum } from "../../../assets/types/header";
import Title from "../../../components/Title/Title";
import AppButton from "../../../components/UI/buttons/app/AppButton";
import scss from "./homeCilab.module.scss";

const HomeCilab = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.CILAB}>
      <Title>
        Ci<span className={scss.lab}>Lab</span>
      </Title>
      <div className={scss.content}>
        <aside className={scss.about}>
          <p className={scss.desc}>
            CILab – центр национальной технологической инициативы (НТИ),
            получивший государственную поддержку по направлению «Технологии
            машинного обучения и когнитивные технологии».{" "}
          </p>
          <p className={scss.info}>
            Мы изучаем проблемы и выявляем, когда мы работаем вместе с нашими
            клиентами, новые решения находят, не задумываясь. Работая таким
            образом и проявляя гибкость, мы можем поддерживать новый подход для
            удовлетворения потребностей наших клиентов.
          </p>
          <AppButton buttonName="О лаборатории" className={scss.button} />
        </aside>
        <ul className={scss.grid}>
          {DATA_CILAB.map((card) => (
            <li
              style={{ backgroundImage: `url(${card.backgroundImage})` }}
              key={card.id}
            />
          ))}
        </ul>
      </div>
    </section>
  );
};

export default HomeCilab;
