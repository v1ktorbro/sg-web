import HomeIntro from "./Intro/HomeIntro";
import HomeAbout from "./About/HomeAbout";
import HomeProjects from "./Projects/HomeProjects";
import HomeTeam from "./Team/HomeTeam";
import HomeCilab from "./Cilab/HomeCilab";
import HomePublications from "./Publications/HomePublications";
import HomeEducation from "./Education/HomeEducation";
import HomePartners from "./Partners/HomePartners";
import HomeContacts from "./Contacts/HomeContacts";

const HomePage = () => {
  return (
    <main>
      <HomeIntro />
      <HomeAbout />
      <HomeCilab />
      <HomeProjects />
      <HomeEducation />
      <HomePublications />
      <HomeTeam />
      <HomePartners />
      <HomeContacts />
    </main>
  );
};

export default HomePage;
