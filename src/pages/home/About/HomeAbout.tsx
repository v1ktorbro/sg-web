import { SectionsIdsEnum } from "../../../assets/types/header";
import Title from "../../../components/Title/Title";
import HomeCardAbout from "./card/HomeCardAbout";
import scss from "./homeAbout.module.scss";
import { DATA_ABOUT } from "../../../assets/data/about";

const HomeAbout = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.ABOUT}>
      <Title>
        Несколько слов&nbsp;<span>о нас</span>
      </Title>

      <div className={scss.container}>
        <p className={scss.desc}>
          Национальный центр когнитивных разработок – центр национальной
          технологической инициативы (НТИ), получивший государственную поддержку
          по направлению «Технологии машинного обучения и когнитивные
          технологии».
        </p>
        <ul className={scss.list}>
          {DATA_ABOUT.map((cardData) => (
            <li key={cardData.id}>
              <HomeCardAbout data={cardData} />
            </li>
          ))}
        </ul>
      </div>
    </section>
  );
};

export default HomeAbout;
