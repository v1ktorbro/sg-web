declare namespace HomeCardAboutModuleScssNamespace {
  export interface IHomeCardAboutModuleScss {
    button: string;
    desc: string;
    icon: string;
    root: string;
    svg: string;
    title: string;
  }
}

declare const HomeCardAboutModuleScssModule: HomeCardAboutModuleScssNamespace.IHomeCardAboutModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardAboutModuleScssNamespace.IHomeCardAboutModuleScss;
};

export = HomeCardAboutModuleScssModule;
