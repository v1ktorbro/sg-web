import { ComponentType, FC } from "react";
import scss from "./homeCardAbout.module.scss";

const HomeCardAbout: FC<{
  data: {
    title: string;
    IconType: ComponentType<{ className?: string }>;
    description: string;
    bacgroundImage: string;
  };
}> = ({ data }) => {
  return (
    <article
      style={{ backgroundImage: `url(${data.bacgroundImage})` }}
      className={scss.root}
    >
      <h3 className={scss.title}>{data.title}</h3>
      <span className={scss.icon}>
        <data.IconType className={scss.svg} />
      </span>
      <p className={scss.desc}>{data.description}</p>
      <button className={scss.button}>Подробнее</button>
    </article>
  );
};

export default HomeCardAbout;
