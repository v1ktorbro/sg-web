import { SectionsIdsEnum } from "../../../assets/types/header";
import Title from "../../../components/Title/Title";
import HomeCardEducation from "./card/HomeCardEducation";
import HomeNavEducation from "./nav/HomeNavEducation";
import scss from "./homeEducation.module.scss";
import { DATA_EDUCATION } from "../../../assets/data/education";
import AppButton from "../../../components/UI/buttons/app/AppButton";

const HomeEducation = () => {
  // временный вариант отображения карточки
  const renderCard = DATA_EDUCATION[0];

  return (
    <section className={scss.root} id={SectionsIdsEnum.EDUCATION}>
      <Title>
        Обр<span>а</span>зование
      </Title>
      <p className={scss.desc}>
        В образовательной деятельности НЦКР активно использует существующую в
        Университете ИТМО инфраструктуру и опыт реализации сетевых форм
        обучения, опираясь на вузы-диссеминаторы (ННГУ, ДВФУ, ФГБОУ ВО «МГТУ
        «СТАНКИН», НовГУ), а также профильную компанию ООО «НЦЭО» в области
        электронного и дистанционного образования. Реализуются образовательные
        программы различных форматов.
      </p>
      <HomeCardEducation data={renderCard} />
      <HomeNavEducation />
      <AppButton buttonName="Подробнее" className={scss.button} />
    </section>
  );
};

export default HomeEducation;
