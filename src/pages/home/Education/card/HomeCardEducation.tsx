import { FC } from "react";
import { cl } from "../../../../assets/utils/classnames";
import scss from "./homeCardEducation.module.scss";

const HomeCardEducation: FC<{
  data: {
    title: string;
    description: string;
    bacgroundImages: Array<string>;
  };
  className?: string;
}> = ({ data, className }) => {
  return (
    <article className={cl(scss.root, className)}>
      <div className={scss.info}>
        <h4 className={scss.title}>{data.title}</h4>
        <p className={scss.desc}>{data.description}</p>
      </div>
      <div className={scss.images}>
        {data.bacgroundImages.map((linkStringImage, index) => (
          <span
            key={index}
            style={{ backgroundImage: `url(${linkStringImage})` }}
          />
        ))}
      </div>
    </article>
  );
};

export default HomeCardEducation;
