declare namespace HomeCardEducationModuleScssNamespace {
  export interface IHomeCardEducationModuleScss {
    desc: string;
    images: string;
    info: string;
    root: string;
    title: string;
  }
}

declare const HomeCardEducationModuleScssModule: HomeCardEducationModuleScssNamespace.IHomeCardEducationModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardEducationModuleScssNamespace.IHomeCardEducationModuleScss;
};

export = HomeCardEducationModuleScssModule;
