declare namespace HomeNavEducationModuleScssNamespace {
  export interface IHomeNavEducationModuleScss {
    checkbox: string;
    hiddenStandartCheckbox: string;
    label: string;
    "r-n": string;
    "r-p": string;
    root: string;
    value: string;
  }
}

declare const HomeNavEducationModuleScssModule: HomeNavEducationModuleScssNamespace.IHomeNavEducationModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeNavEducationModuleScssNamespace.IHomeNavEducationModuleScss;
};

export = HomeNavEducationModuleScssModule;
