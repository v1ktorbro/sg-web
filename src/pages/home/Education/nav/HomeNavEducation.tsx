import scss from "./homeNavEducation.module.scss";
import { MouseEvent } from "react";
import { cl } from "../../../../assets/utils/classnames";
import { ItemsOnHoverIdsEnum } from "../../../../assets/types/header";

const HomeNavEducation = () => {
  const handlerCheckbox = (event: MouseEvent | TouchEvent) => {
    const elementValue = event.target as HTMLSpanElement;
    console.log(elementValue.id);
    return elementValue.id;
  };

  return (
    <nav className={scss.root}>
      <input
        className={cl(scss.checkbox, scss.hiddenStandartCheckbox)}
        id="io"
        type="checkbox"
        // defaultChecked={false}
      />
      <label className={scss.label} htmlFor="io">
        <span
          role="button"
          onKeyDown={() => handlerCheckbox}
          tabIndex={0}
          id={ItemsOnHoverIdsEnum.PHDSTUDENT}
          onClick={handlerCheckbox}
          className={scss.value}
          data-value="Аспирантура"
        />
        <span
          role="button"
          tabIndex={0}
          onKeyDown={() => handlerCheckbox}
          onClick={handlerCheckbox}
          id={ItemsOnHoverIdsEnum.MAGISTRACY}
          className={scss.value}
          data-value="Магистратура"
        />
      </label>
    </nav>
  );
};

export default HomeNavEducation;
