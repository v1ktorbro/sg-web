import { DATA_PUBLICATIONS } from "../../../assets/data/publications";
import { SectionsIdsEnum } from "../../../assets/types/header";
import Carousel from "../../../components/Carousel/Carousel";
import Title from "../../../components/Title/Title";
import AppButton from "../../../components/UI/buttons/app/AppButton";
import HomeCardPublications from "./card/HomeCardPublications";
import scss from "./homePublications.module.scss";

const HomePublications = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.PUBLICATIONS}>
      <Title>
        Публик<span>а</span>ции
      </Title>

      <Carousel
        classNameCardsContainer={scss.publications}
        arrData={DATA_PUBLICATIONS}
        Element={(props) => (
          <HomeCardPublications {...props} classname={scss.card} />
        )}
        countCardsView={4}
        countSwipeStep={2}
      />

      <AppButton buttonName="Смотреть всё" className={scss.button} />
    </section>
  );
};

export default HomePublications;
