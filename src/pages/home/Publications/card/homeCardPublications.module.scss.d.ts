declare namespace HomeCardPublicationsModuleScssNamespace {
  export interface IHomeCardPublicationsModuleScss {
    button: string;
    desc: string;
    root: string;
    title: string;
  }
}

declare const HomeCardPublicationsModuleScssModule: HomeCardPublicationsModuleScssNamespace.IHomeCardPublicationsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardPublicationsModuleScssNamespace.IHomeCardPublicationsModuleScss;
};

export = HomeCardPublicationsModuleScssModule;
