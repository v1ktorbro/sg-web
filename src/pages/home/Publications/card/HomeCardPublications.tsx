import { FC } from "react";
import { cl } from "../../../../assets/utils/classnames";
import scss from "./homeCardPublications.module.scss";

const HomeCardPublications: FC<{
  data: {
    title: string;
    description: string;
  };
  classname?: string;
}> = ({ data, classname }) => {
  return (
    <article className={cl(scss.root, classname)}>
      <h4 className={scss.title}>{data.title}</h4>
      <p className={scss.desc}>{data.description}</p>
      <button className={scss.button}>Посмотреть</button>
    </article>
  );
};

export default HomeCardPublications;
