import { FC, useState } from "react";
import { ICardIntro } from "../../../../assets/types/intro";
import { cl } from "../../../../assets/utils/classnames";
import scss from "./homeIntroCardNews.module.scss";

const HomeIntroCardNews: FC<{
  data: ICardIntro;
  className?: string;
}> = ({ data, className }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handlerClick = () => setIsOpen((prev) => !prev);

  return (
    <article className={cl(scss.root, className)}>
      <div className={cl(scss.content, isOpen && scss.opened)}>
        <a href="/#" className={scss.link}>
          <h3 className={scss.title}>{data.title}</h3>
          <p className={scss.desc}>{data.description}</p>
          <span className={scss.date}>{`${data.date}.2022`}</span>
          <span
            className={scss.img}
            style={{ backgroundImage: `url(${data.backgroundImage})` }}
          />
        </a>
      </div>
      <button onClick={handlerClick} className={scss.btn}>
        <span>Последние новости</span>
      </button>
    </article>
  );
};

export default HomeIntroCardNews;
