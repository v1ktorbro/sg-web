declare namespace HomeIntroCardNewsModuleScssNamespace {
  export interface IHomeIntroCardNewsModuleScss {
    btn: string;
    content: string;
    date: string;
    desc: string;
    img: string;
    link: string;
    opened: string;
    root: string;
    title: string;
  }
}

declare const HomeIntroCardNewsModuleScssModule: HomeIntroCardNewsModuleScssNamespace.IHomeIntroCardNewsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeIntroCardNewsModuleScssNamespace.IHomeIntroCardNewsModuleScss;
};

export = HomeIntroCardNewsModuleScssModule;
