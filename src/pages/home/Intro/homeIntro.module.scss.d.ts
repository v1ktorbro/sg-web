declare namespace HomeIntroModuleScssNamespace {
  export interface IHomeIntroModuleScss {
    button: string;
    container: string;
    desc: string;
    root: string;
    title: string;
  }
}

declare const HomeIntroModuleScssModule: HomeIntroModuleScssNamespace.IHomeIntroModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeIntroModuleScssNamespace.IHomeIntroModuleScss;
};

export = HomeIntroModuleScssModule;
