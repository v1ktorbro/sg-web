import { DATA_INTRO_CARD_NEWS } from "../../../assets/data/intro";
import { SectionsIdsEnum } from "../../../assets/types/header";
import AppButton from "../../../components/UI/buttons/app/AppButton";
import HomeIntroCardNews from "./cardNews/HomeIntroCardNews";
import scss from "./homeIntro.module.scss";

const HomeIntro = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.INTRO}>
      <div className={scss.container}>
        <h1 className={scss.title}>
          Когнитивнные технологии, <span>машинное обучение</span>
        </h1>
        <p className={scss.desc}>
          Системы прикладного искусственного интеллекта
        </p>
        <AppButton buttonName="Наши проекты" className={scss.button} />
      </div>
      <HomeIntroCardNews data={DATA_INTRO_CARD_NEWS} />
    </section>
  );
};

export default HomeIntro;
