import { FC } from "react";
import scss from "./homeCardPartners.module.scss";

const HomeCardPartners: FC<{
  data: {
    id: string;
    backgroundImage: string;
  };
}> = ({ data }) => {
  return (
    <div className={scss.root}>
      <span
        key={data.id}
        className={scss.logo}
        style={{ backgroundImage: `url(${data.backgroundImage})` }}
      />
    </div>
  );
};

export default HomeCardPartners;
