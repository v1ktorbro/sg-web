declare namespace HomeCardPartnersModuleScssNamespace {
  export interface IHomeCardPartnersModuleScss {
    logo: string;
    root: string;
  }
}

declare const HomeCardPartnersModuleScssModule: HomeCardPartnersModuleScssNamespace.IHomeCardPartnersModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardPartnersModuleScssNamespace.IHomeCardPartnersModuleScss;
};

export = HomeCardPartnersModuleScssModule;
