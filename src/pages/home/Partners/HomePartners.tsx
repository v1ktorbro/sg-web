import { DATA_PARTNERS } from "../../../assets/data/partners";
import Carousel from "../../../components/Carousel/Carousel";
import Title from "../../../components/Title/Title";
import HomeCardPartners from "./card/HomeCardPartners";
import scss from "./homePartners.module.scss";

const HomePartners = () => {
  return (
    <section className={scss.root}>
      <Title>
        С кем мы <span>работаем</span>?
      </Title>

      <Carousel
        arrData={DATA_PARTNERS}
        Element={(props) => <HomeCardPartners {...props} />}
        classNameCardsContainer={scss.partners}
        classNameComponent={scss.buttons}
        countCardsView={6}
      />

      <p className={scss.desc}>
        За время сотрудничества компания показала себя как надежный партнер,
        способный оперативно решать вопросы по внесению необходимых изменений.
        Отдельно хочется отметить квалифицированный персонал компании, который
        всегда готов прийти на помощь в решении поставленных задач, дать
        объективную оценку и свои рекомендации по улучшению деятельности.
        Надеемся на дальнейшее и плодотворное сотрудничество.
      </p>
    </section>
  );
};

export default HomePartners;
