declare namespace HomeCardTeamModuleScssNamespace {
  export interface IHomeCardTeamModuleScss {
    info: string;
    name: string;
    root: string;
    title: string;
  }
}

declare const HomeCardTeamModuleScssModule: HomeCardTeamModuleScssNamespace.IHomeCardTeamModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardTeamModuleScssNamespace.IHomeCardTeamModuleScss;
};

export = HomeCardTeamModuleScssModule;
