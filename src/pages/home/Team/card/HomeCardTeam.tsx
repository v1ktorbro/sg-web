import { FC } from "react";
import scss from "./homeCardTeam.module.scss";

const HomeCardTeam: FC<{
  data: {
    id: number;
    name: string;
    title: string;
  };
}> = ({ data }) => {
  return (
    <article className={scss.root}>
      <div className={scss.info}>
        <h3 className={scss.name}>{data.name}</h3>
        <p className={scss.title}>{data.title}</p>
      </div>
    </article>
  );
};

export default HomeCardTeam;
