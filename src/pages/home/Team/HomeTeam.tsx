import { DATA_TEAM } from "../../../assets/data/team";
import scss from "./homeTeam.module.scss";
import Title from "../../../components/Title/Title";
import Carousel from "../../../components/Carousel/Carousel";
import HomeCardTeam from "./card/HomeCardTeam";
import { SectionsIdsEnum } from "../../../assets/types/header";

const HomeTeam = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.TEAM}>
      <Title>
        Наша&nbsp;<span>команда</span>
      </Title>

      <Carousel
        classNameCardsContainer={scss.team}
        arrData={DATA_TEAM}
        Element={HomeCardTeam}
      />
    </section>
  );
};

export default HomeTeam;
