declare namespace HomeFeedBackContactsModuleScssNamespace {
  export interface IHomeFeedBackContactsModuleScss {
    button: string;
    email: string;
    input: string;
    name: string;
    phone: string;
    root: string;
    textarea: string;
  }
}

declare const HomeFeedBackContactsModuleScssModule: HomeFeedBackContactsModuleScssNamespace.IHomeFeedBackContactsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeFeedBackContactsModuleScssNamespace.IHomeFeedBackContactsModuleScss;
};

export = HomeFeedBackContactsModuleScssModule;
