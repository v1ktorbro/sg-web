import { cl } from "../../../../assets/utils/classnames";
import scss from "./homeFeedBackContacts.module.scss";

const HomeFeedBackContacts = () => {
  return (
    <article className={scss.root}>
      <input
        className={cl(scss.input, scss.name)}
        type="text"
        placeholder="Имя"
      />
      <input
        className={cl(scss.input, scss.phone)}
        type="number"
        placeholder="Телефон"
      />
      <input
        className={cl(scss.input, scss.email)}
        type="email"
        placeholder="E-mail"
      />
      <button className={scss.button}>Отправить</button>
      <textarea
        className={scss.textarea}
        name="message"
        id="message"
        placeholder="Сообщение"
      />
    </article>
  );
};

export default HomeFeedBackContacts;
