import scss from "./homeAdressContacts.module.scss";

const HomeAdressContacts = () => {
  return (
    <article className={scss.root}>
      <h3 className={scss.city}>г.Санкт-Петербург</h3>
      <p className={scss.adress}>
        199034, Санкт-Петербург, Биржевая линия, д. 4, лит. М, ауд. 302С
      </p>
      <span className={scss.phone}>
        +7 (812) 337-64-90 <br /> +7 (812) 337-64-94
      </span>
      <span className={scss.email}>anna.lutsenko@itmo.ru</span>
    </article>
  );
};

export default HomeAdressContacts;
