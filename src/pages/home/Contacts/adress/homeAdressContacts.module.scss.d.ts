declare namespace HomeAdressContactsModuleScssNamespace {
  export interface IHomeAdressContactsModuleScss {
    adress: string;
    city: string;
    email: string;
    phone: string;
    root: string;
  }
}

declare const HomeAdressContactsModuleScssModule: HomeAdressContactsModuleScssNamespace.IHomeAdressContactsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeAdressContactsModuleScssNamespace.IHomeAdressContactsModuleScss;
};

export = HomeAdressContactsModuleScssModule;
