import { SectionsIdsEnum } from "../../../assets/types/header";
import Title from "../../../components/Title/Title";
import scss from "./homeContacts.module.scss";
import HomeAdressContacts from "./adress/HomeAdressContacts";
import HomeFeedBackContacts from "./feedbcack/HomeFeedBackContacts";

const HomeContacts = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.CONTACTS}>
      <Title>Контакты</Title>
      <aside className={scss.info}>
        <HomeAdressContacts />
        <p className={scss.message}>
          По всем интересующим вас вопросам вы всегда можете обратиться по
          указанным телефонам, написать на почту или оставтиь свое сообщение
          через форму обратной связи.
        </p>
        <HomeFeedBackContacts />
      </aside>
    </section>
  );
};

export default HomeContacts;
