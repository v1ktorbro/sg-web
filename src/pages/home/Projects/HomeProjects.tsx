import { SectionsIdsEnum } from "../../../assets/types/header";
import { DATA_PROJECTS } from "../../../assets/data/projects";
import Carousel from "../../../components/Carousel/Carousel";
import Title from "../../../components/Title/Title";
import HomeCardProject from "./card/HomeCardProject";
import scss from "./homeProjects.module.scss";

const HomeProjects = () => {
  return (
    <section className={scss.root} id={SectionsIdsEnum.PROJECT}>
      <Title>
        Наши&nbsp;<span>проекты</span>
      </Title>

      <Carousel
        arrData={DATA_PROJECTS}
        Element={(props) => (
          <HomeCardProject {...props} className={scss.card} />
        )}
        classNameCardsContainer={scss.projects}
        countCardsView={8}
        countSwipeStep={2}
      />
    </section>
  );
};

export default HomeProjects;
