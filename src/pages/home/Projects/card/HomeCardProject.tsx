import { FC } from "react";
import { cl } from "../../../../assets/utils/classnames";
import scss from "./homeCardProject.module.scss";

const HomeCardProject: FC<{
  data: {
    title: string;
    logo: string;
    backgroundImageCard: string;
  };
  className?: string;
}> = ({ data, className }) => {
  return (
    <article
      className={cl(scss.root, className)}
      // style={{ backgroundImage: `url(${data.backgroundImageCard})` }}
    >
      <span
        className={scss.backgroundImage}
        style={{ backgroundImage: `url(${data.backgroundImageCard})` }}
      />

      <span
        className={scss.logo}
        style={{ backgroundImage: `url(${data.logo})` }}
      />

      <p className={scss.title}>{data.title}</p>
      <button className={scss.button}>Подробнее</button>
    </article>
  );
};

export default HomeCardProject;
