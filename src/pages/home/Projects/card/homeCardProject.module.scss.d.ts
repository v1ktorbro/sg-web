declare namespace HomeCardProjectModuleScssNamespace {
  export interface IHomeCardProjectModuleScss {
    backgroundImage: string;
    button: string;
    logo: string;
    root: string;
    title: string;
  }
}

declare const HomeCardProjectModuleScssModule: HomeCardProjectModuleScssNamespace.IHomeCardProjectModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HomeCardProjectModuleScssNamespace.IHomeCardProjectModuleScss;
};

export = HomeCardProjectModuleScssModule;
