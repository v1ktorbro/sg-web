import { FC } from "react";
import { DATA_SOCIALS } from "../../assets/data/socials";
import { cl } from "../../assets/utils/classnames";
import scss from "./socials.module.scss";

const Socials: FC<{
  className?: string;
}> = ({ className }) => {
  return (
    <nav className={cl(scss.root, className)}>
      <ul className={scss.list}>
        {DATA_SOCIALS.map(({ IconType, ...socialItem }) => (
          <li className={scss.icon} key={socialItem.id}>
            <IconType className={scss.svg} />
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Socials;
