declare namespace SocialsModuleScssNamespace {
  export interface ISocialsModuleScss {
    icon: string;
    list: string;
    root: string;
    svg: string;
  }
}

declare const SocialsModuleScssModule: SocialsModuleScssNamespace.ISocialsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: SocialsModuleScssNamespace.ISocialsModuleScss;
};

export = SocialsModuleScssModule;
