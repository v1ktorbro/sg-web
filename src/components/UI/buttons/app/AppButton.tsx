import { FC } from "react";
import { cl } from "../../../../assets/utils/classnames";
import scss from "./appButton.module.scss";

const AppButton: FC<{
  buttonName: string;
  className?: string;
}> = ({ buttonName, className }) => {
  return <button className={cl(scss.root, className)}>{buttonName}</button>;
};

export default AppButton;
