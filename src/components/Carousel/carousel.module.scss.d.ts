declare namespace CarouselModuleScssNamespace {
  export interface ICarouselModuleScss {
    button: string;
    nextSlide: string;
    prevSlide: string;
    root: string;
  }
}

declare const CarouselModuleScssModule: CarouselModuleScssNamespace.ICarouselModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: CarouselModuleScssNamespace.ICarouselModuleScss;
};

export = CarouselModuleScssModule;
