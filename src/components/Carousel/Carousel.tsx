import { FC, ReactElement, useEffect, useState } from "react";
import { cl } from "../../assets/utils/classnames";
import scss from "./carousel.module.scss";

const Carousel: <T extends { id: string | number }>(props: {
  arrData: T[];
  Element: FC<{ data: T }>;
  countCardsView?: number;
  countSwipeStep?: number;
  classNameCardsContainer?: string;
  classNameComponent?: string;
}) => ReactElement = ({
  arrData,
  Element,
  countCardsView = 4,
  countSwipeStep = 1,
  classNameCardsContainer,
  classNameComponent,
}) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  const [arrForRender, setArrForRender] = useState(
    arrData.slice(currentIndex, currentIndex + countCardsView)
  );

  const handlerNextButton = () =>
    setCurrentIndex((prev) => prev + countSwipeStep);

  const handlerPrevButton = () =>
    setCurrentIndex((prev) => prev - countSwipeStep);

  useEffect(() => {
    setArrForRender(arrData.slice(currentIndex, currentIndex + countCardsView));
  }, [arrData, countCardsView, currentIndex]);

  return (
    <section className={cl(scss.root, classNameComponent)}>
      <button
        disabled={currentIndex < 1}
        onClick={handlerPrevButton}
        id={`prevSlide`}
        className={cl(scss.button, scss.prevSlide)}
      >
        &#8249;
      </button>

      <aside className={classNameCardsContainer}>
        {arrForRender.map((item) => (
          <Element key={item.id} data={item} />
        ))}
      </aside>

      <button
        disabled={currentIndex + countCardsView + 1 > arrData.length}
        onClick={handlerNextButton}
        id={`nextSlide`}
        className={cl(scss.button, scss.nextSlide)}
      >
        &#8250;
      </button>
    </section>
  );
};

export default Carousel;
