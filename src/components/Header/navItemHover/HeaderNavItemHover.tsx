import { DATA_HEADER_HOVER_ITEM_LIST } from "../../../assets/data/header";
import scss from "./headerNavItemHover.module.scss";

const HeaderNavItemHover = () => {
  return (
    <ul className={scss.root}>
      {DATA_HEADER_HOVER_ITEM_LIST.map((item) => (
        <li className={scss.item} key={item.id}>
          <a className={scss.link} href={`#${item.id}`}>
            {item.name}
          </a>
        </li>
      ))}
    </ul>
  );
};

export default HeaderNavItemHover;
