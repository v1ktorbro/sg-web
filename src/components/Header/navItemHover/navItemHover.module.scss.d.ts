declare namespace NavItemHoverModuleScssNamespace {
  export interface INavItemHoverModuleScss {
    item: string;
    link: string;
    root: string;
  }
}

declare const NavItemHoverModuleScssModule: NavItemHoverModuleScssNamespace.INavItemHoverModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: NavItemHoverModuleScssNamespace.INavItemHoverModuleScss;
};

export = NavItemHoverModuleScssModule;
