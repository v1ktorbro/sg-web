declare namespace HeaderNavItemHoverModuleScssNamespace {
  export interface IHeaderNavItemHoverModuleScss {
    item: string;
    link: string;
    root: string;
  }
}

declare const HeaderNavItemHoverModuleScssModule: HeaderNavItemHoverModuleScssNamespace.IHeaderNavItemHoverModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: HeaderNavItemHoverModuleScssNamespace.IHeaderNavItemHoverModuleScss;
};

export = HeaderNavItemHoverModuleScssModule;
