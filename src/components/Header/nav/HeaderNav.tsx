import scss from "./headerNav.module.scss";
import HeaderNavItemHover from "../navItemHover/HeaderNavItemHover";
import { MouseEvent, useState } from "react";
import { DATA_HEADER_MENU_LIST } from "../../../assets/data/header";

const HeaderNav = () => {
  const [targetNav, setTargetNav] = useState({
    isTarget: false,
    nameTarget: "Образование",
  });

  const handlerTargetItemList = (event: MouseEvent<HTMLLIElement>) => {
    const value = event.target as HTMLElement;
    value.textContent === targetNav.nameTarget
      ? setTargetNav({ ...targetNav, isTarget: true })
      : setTargetNav({ ...targetNav, isTarget: false });
  };

  return (
    <nav className={scss.root}>
      <ul className={scss.list}>
        {DATA_HEADER_MENU_LIST.map((menuItem) => (
          <li
            onMouseEnter={handlerTargetItemList}
            className={scss.item}
            key={menuItem.id}
          >
            <a className={scss.link} href={`#${menuItem.id}`}>
              {menuItem.name}
            </a>
            {menuItem.name === targetNav.nameTarget && targetNav.isTarget && (
              <HeaderNavItemHover />
            )}
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default HeaderNav;
