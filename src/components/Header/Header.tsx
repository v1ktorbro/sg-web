import { Link } from "react-router-dom";
import { AppRoutes } from "../../router/routers";
import Socials from "../Socials/Socials";
import scss from "./header.module.scss";
import HeaderNav from "./nav/HeaderNav";

const Header = () => (
  <header className={scss.root}>
    <div className={scss.wrapper}>
      <Link to={AppRoutes.HOME} className={scss.logo}>
        <span className={scss.img} />
        {`<sg web>`.toUpperCase()}
      </Link>
    </div>
    <HeaderNav />
    <Socials />
  </header>
);

export default Header;
