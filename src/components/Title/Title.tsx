import { FC, ReactNode } from "react";
import { cl } from "../../assets/utils/classnames";
import scss from "./title.module.scss";

const Title: FC<{
  children: ReactNode;
  className?: string;
}> = ({ children, className }) => {
  return <h2 className={cl(scss.root, className)}>{children}</h2>;
};

export default Title;
