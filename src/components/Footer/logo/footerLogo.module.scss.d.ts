declare namespace FooterLogoModuleScssNamespace {
  export interface IFooterLogoModuleScss {
    img: string;
    info: string;
    logo: string;
    root: string;
    socials: string;
  }
}

declare const FooterLogoModuleScssModule: FooterLogoModuleScssNamespace.IFooterLogoModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: FooterLogoModuleScssNamespace.IFooterLogoModuleScss;
};

export = FooterLogoModuleScssModule;
