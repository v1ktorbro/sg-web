import { Link } from "react-router-dom";
import { AppRoutes } from "../../../router/routers";
import Socials from "../../Socials/Socials";
import scss from "./footerLogo.module.scss";

const FooterNav = () => {
  return (
    <aside className={scss.root}>
      <Link to={AppRoutes.HOME} className={scss.logo}>
        <span className={scss.img} />
        {`<sg web>`.toUpperCase()}
      </Link>
      <span className={scss.info}>
        Информация © 2020 <br /> Университет ИТМО
      </span>
      <Socials className={scss.socials} />
    </aside>
  );
};

export default FooterNav;
