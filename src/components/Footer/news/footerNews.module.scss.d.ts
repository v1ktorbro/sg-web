declare namespace FooterNewsModuleScssNamespace {
  export interface IFooterNewsModuleScss {
    cardTitle: string;
    date: string;
    hoverImg: string;
    img: string;
    link: string;
    list: string;
    root: string;
    title: string;
  }
}

declare const FooterNewsModuleScssModule: FooterNewsModuleScssNamespace.IFooterNewsModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: FooterNewsModuleScssNamespace.IFooterNewsModuleScss;
};

export = FooterNewsModuleScssModule;
