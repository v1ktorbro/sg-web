import { FC } from "react";
import { IFooterNewsCard } from "../../../assets/types/footer";
import { cl } from "../../../assets/utils/classnames";
import scss from "./footerNews.module.scss";

const FooterNews: FC<{
  arrData: IFooterNewsCard[];
  className?: string;
}> = ({ arrData, className }) => {
  return (
    <aside className={cl(scss.root, className)}>
      <h3 className={scss.title}>Последние новости</h3>
      <ul className={scss.list}>
        {arrData.map((card) => (
          <li key={card.id}>
            <a href="/#" className={scss.link}>
              <span
                className={scss.img}
                style={{ backgroundImage: `url(${card.backgroundImage})` }}
              />
              <span className={scss.hoverImg} />
              <h4 className={scss.cardTitle}>{card.title}</h4>
              <span className={scss.date}>{`${card.date}.2020`}</span>
            </a>
          </li>
        ))}
      </ul>
    </aside>
  );
};

export default FooterNews;
