import {
  DATA_FOOTER_LIST_ABOUT,
  DATA_FOOTER_LIST_DEVELOPMENT,
  DATA_FOOTER_LIST_NEWS,
} from "../../assets/data/footer";
import FooterListLinks from "./listLinks/FooterListLinks";
import scss from "./footer.module.scss";
import FooterLogo from "./logo/FooterLogo";
import FooterNews from "./news/FooterNews";

const Footer = () => (
  <footer className={scss.root}>
    <FooterLogo />

    <p className={scss.desc}>
      Национальный центр когнитивных разработок – центр национальной
      технологической инициативы (НТИ), получивший государственную поддержку по
      направлению «Технологии машинного обучения и когнитивные технологии».
      Инфраструктурная основа взаимодействия научных, образовательных и
      промышленных организаций
    </p>

    <FooterListLinks
      title="О центре"
      arrData={DATA_FOOTER_LIST_ABOUT}
      className={scss.about}
    />

    <FooterListLinks
      title="Развитие"
      arrData={DATA_FOOTER_LIST_DEVELOPMENT}
      className={scss.development}
    />

    <FooterNews arrData={DATA_FOOTER_LIST_NEWS} />
  </footer>
);

export default Footer;
