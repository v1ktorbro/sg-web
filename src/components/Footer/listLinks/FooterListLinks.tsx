import { FC } from "react";
import {
  IFooterAboutLink,
  IFooterDevelopmentLink,
} from "../../../assets/types/footer";
import { cl } from "../../../assets/utils/classnames";
import scss from "./footerListLinks.module.scss";

const FooterListLinks: FC<{
  arrData: IFooterAboutLink[] | IFooterDevelopmentLink[];
  title: string;
  className?: string;
}> = ({ arrData, title, className }) => {
  return (
    <nav className={cl(scss.root, className)}>
      <h3 className={scss.title}>{title}</h3>
      <ul className={scss.list}>
        {arrData.map((item) => (
          <li key={item.id} className={scss.item}>
            <a className={scss.link} id={item.id} href={`#${item.id}`}>
              {item.name}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default FooterListLinks;
