declare namespace FooterListLinksModuleScssNamespace {
  export interface IFooterListLinksModuleScss {
    item: string;
    link: string;
    list: string;
    root: string;
    title: string;
  }
}

declare const FooterListLinksModuleScssModule: FooterListLinksModuleScssNamespace.IFooterListLinksModuleScss & {
  /** WARNING: Only available when `css-loader` is used without `style-loader` or `mini-css-extract-plugin` */
  locals: FooterListLinksModuleScssNamespace.IFooterListLinksModuleScss;
};

export = FooterListLinksModuleScssModule;
