export interface ICardIntro {
  id: number;
  title: string;
  description: string;
  date: number;
  backgroundImage: string;
}
