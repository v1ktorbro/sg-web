import { ItemsOnHoverIdsEnum, SectionsIdsEnum } from "./header";

export interface IFooterAboutLink {
  name: string;
  id: SectionsIdsEnum;
}

export interface IFooterDevelopmentLink {
  name: string;
  id: SectionsIdsEnum | ItemsOnHoverIdsEnum;
}

export interface IFooterNewsCard {
  id: number;
  title: string;
  date: number;
  backgroundImage: string;
}
