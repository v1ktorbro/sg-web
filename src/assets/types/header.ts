export interface INavLink {
  name: string;
  id: SectionsIdsEnum;
}

export enum SectionsIdsEnum {
  INTRO = "intro",
  ABOUT = "about",
  CILAB = "cilab",
  PROJECT = "project",
  EDUCATION = "education",
  PUBLICATIONS = "publications",
  TEAM = "team",
  CONTACTS = "contacts",
}

export interface IHoverNavLink {
  name: string;
  id: ItemsOnHoverIdsEnum;
}

export enum ItemsOnHoverIdsEnum {
  PHDSTUDENT = "phdstudent",
  MAGISTRACY = "magistracy",
}
