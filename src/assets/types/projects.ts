export interface ICardProject {
  id: number;
  title: string;
  logo: string;
  backgroundImageCard: string;
}
