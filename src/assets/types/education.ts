export interface ICardEducation {
  id: string;
  title: string;
  description: string;
  bacgroundImages: string[];
  className?: string;
}
