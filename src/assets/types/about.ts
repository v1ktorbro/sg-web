import { ComponentType } from "react";

export interface ICardAbout {
  id: AboutCardsIdsEnum;
  title: string;
  IconType: ComponentType<{ className?: string }>;
  description: string;
  bacgroundImage: string;
}

export enum AboutCardsIdsEnum {
  DESIGNING = "designing",
  MACHINELEARNING = "machine-learning",
  DATABASES = "data-bases",
  ROBOTICS = "robotics",
}
