import { ComponentType } from "react";

export interface ISocialLink {
  id: SocailsIdsEnum;
  IconType: ComponentType<{ className?: string }>;
}

export enum SocailsIdsEnum {
  FACEBOOK = "facebook",
  TELEGRAM = "telegram",
  GOOGLE = "google",
  LINKEDIN = "linkedin",
}
