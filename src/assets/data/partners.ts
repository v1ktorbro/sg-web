import Gazprom from "../images/partners/gazprom.png";
import Sberbank from "../images/partners/sberbank.png";

interface I {
  id: string;
  backgroundImage: string;
}

export const DATA_PARTNERS: I[] = [
  { id: "sberbank1", backgroundImage: Sberbank },
  { id: "gazprom1", backgroundImage: Gazprom },
  { id: "sberbank2", backgroundImage: Sberbank },
  { id: "gazprom2", backgroundImage: Gazprom },
  { id: "sberbank3", backgroundImage: Sberbank },
  { id: "gazprom3", backgroundImage: Gazprom },
  { id: "sberbank4", backgroundImage: Sberbank },
  { id: "gazprom4", backgroundImage: Gazprom },
  { id: "sberbank5", backgroundImage: Sberbank },
];
