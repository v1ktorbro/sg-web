import Cilab_image_1 from "../images/cilab/cilab-1.jpg";
import Cilab_image_2 from "../images/cilab/cilab-2.jpg";
import Cilab_image_3 from "../images/cilab/cilab-3.jpg";
import Cilab_image_4 from "../images/cilab/cilab-4.jpg";
import Cilab_image_5 from "../images/cilab/cilab-5.jpg";
import Cilab_image_6 from "../images/cilab/cilab-6.jpg";
import Cilab_image_7 from "../images/cilab/cilab-7.jpg";
import Cilab_image_8 from "../images/cilab/cilab-8.jpg";

interface I {
  id: number;
  backgroundImage: string;
}

export const DATA_CILAB: I[] = [
  { id: 1, backgroundImage: Cilab_image_1 },
  { id: 2, backgroundImage: Cilab_image_2 },
  { id: 3, backgroundImage: Cilab_image_3 },
  { id: 4, backgroundImage: Cilab_image_4 },
  { id: 5, backgroundImage: Cilab_image_5 },
  { id: 6, backgroundImage: Cilab_image_6 },
  { id: 7, backgroundImage: Cilab_image_7 },
  { id: 8, backgroundImage: Cilab_image_8 },
];
