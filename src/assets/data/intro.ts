import { ICardIntro } from "../types/intro";

import Intro_card_news_image from "../images/intro/intro-journal-epi.png";

export const DATA_INTRO_CARD_NEWS: ICardIntro = {
  id: 1,
  title:
    "Журнал «Научно-технический вестник информационных технологий, механики и оптики»",
  description:
    "Обзорная статья, посвященная актуальным фундаментальным проблемам науки о наносистемах.",
  date: parseFloat("12.06.2020"),
  backgroundImage: Intro_card_news_image,
};
