import DesigningIcon from "@mui/icons-material/SportsEsports";
import MachineLearningIcon from "@mui/icons-material/Psychology";
import DatabasesIcon from "@mui/icons-material/Ballot";
import RoboticsIcon from "@mui/icons-material/Android";

import DesigningImage from "../images/about/designing.png";
import MachineLearningImage from "../images/about/machine-learning.png";
import DatabasesImage from "../images/about/data-base.png";
import RoboticsImage from "../images/about/robotics.png";

import { AboutCardsIdsEnum, ICardAbout } from "../types/about";

export const DATA_ABOUT: ICardAbout[] = [
  {
    id: AboutCardsIdsEnum.DESIGNING,
    title: "Проектирование",
    description:
      "Направление включает в себя исследования и разработки технологий МО и КТ",
    IconType: DesigningIcon,
    bacgroundImage: DesigningImage,
  },
  {
    id: AboutCardsIdsEnum.MACHINELEARNING,
    title: "Машинное обучение",
    description:
      "Направление включает в себя исследования и разработки технологий МО и КТ",
    IconType: MachineLearningIcon,
    bacgroundImage: MachineLearningImage,
  },
  {
    id: AboutCardsIdsEnum.DATABASES,
    title: "Базы данных",
    description:
      "Направление включает в себя исследования и разработки технологий МО и КТ",
    IconType: DatabasesIcon,
    bacgroundImage: DatabasesImage,
  },
  {
    id: AboutCardsIdsEnum.ROBOTICS,
    title: "Робототехника",
    description:
      "Направление включает в себя исследования и разработки технологий МО и КТ",
    IconType: RoboticsIcon,
    bacgroundImage: RoboticsImage,
  },
];
