interface I {
  id: number;
  title: string;
  description: string;
}

export const DATA_PUBLICATIONS: I[] = [
  {
    id: 1,
    title:
      "11111Improving the efficiency of noise resistance processing of speech signal",
    description:
      "11111ImprovingAbdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 2,
    title:
      "222222Improving the efficiency of noise resistance processing of speech signal",
    description:
      "222222Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 3,
    title:
      "333333Improving the efficiency of noise resistance processing of speech signal",
    description:
      "222222Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 4,
    title:
      "44444Improving the efficiency of noise resistance processing of speech signal",
    description:
      "44444Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 5,
    title:
      "55555Improving the efficiency of noise resistance processing of speech signal",
    description:
      "55555Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 6,
    title:
      "6666Improving the efficiency of noise resistance processing of speech signal",
    description:
      "6666Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 7,
    title:
      "7777Improving the efficiency of noise resistance processing of speech signal",
    description:
      "7777Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 8,
    title:
      "8888888Improving the efficiency of noise resistance processing of speech signal",
    description:
      "8888888Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 9,
    title:
      "999999Improving the efficiency of noise resistance processing of speech signal",
    description:
      "999999Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 10,
    title:
      "1010101010Improving the efficiency of noise resistance processing of speech signal",
    description:
      "1010101010Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
  {
    id: 11,
    title:
      "11111111Improving the efficiency of noise resistance processing of speech signal",
    description:
      "11111111Improving Abdulkhairov M.T., Altay Y.A., Zhumasheva Z.T. Proceedings of the 2017 IEEE Russia, Section Young Researchers in Electrical and Electronic Engineering Conference, ElConRus 2017 - 2017, pp. 618-620",
  },
];
