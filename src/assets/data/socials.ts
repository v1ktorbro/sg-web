import FacebookIcon from "@mui/icons-material/Facebook";
import TelegramIcon from "@mui/icons-material/Telegram";
import GoogleIcon from "@mui/icons-material/Google";
import LinkedInIcon from "@mui/icons-material/LinkedIn";

import { ISocialLink, SocailsIdsEnum } from "../../assets/types/socials";

export const DATA_SOCIALS: ISocialLink[] = [
  {
    id: SocailsIdsEnum.FACEBOOK,
    IconType: FacebookIcon,
  },
  {
    id: SocailsIdsEnum.TELEGRAM,
    IconType: TelegramIcon,
  },
  {
    id: SocailsIdsEnum.GOOGLE,
    IconType: GoogleIcon,
  },
  {
    id: SocailsIdsEnum.LINKEDIN,
    IconType: LinkedInIcon,
  },
];
