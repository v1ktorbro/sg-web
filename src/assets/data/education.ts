import Education_image_1 from "../images/education/education-1.jpg";
import Education_image_2 from "../images/education/education-2.jpg";
import { ICardEducation } from "../types/education";
import { ItemsOnHoverIdsEnum } from "../types/header";

export const DATA_EDUCATION: ICardEducation[] = [
  {
    id: ItemsOnHoverIdsEnum.PHDSTUDENT,
    title: "Образовательные программы аспирантуры в области МО и КТ",
    description:
      "Прикладные технологии искусственного интеллекта; Машинное обучение и искусственный интеллект; Биометрические информационные системы; Речевые информационные системы; Когнитивные вычисления и квантовый интеллект; Большие данные и экстренные вычисления; Семантические системы и инженерия знаний; Технологии разработки компьютерных игр",
    bacgroundImages: [Education_image_1, Education_image_2],
  },
  {
    id: ItemsOnHoverIdsEnum.MAGISTRACY,
    title: "Разработка программного обеспечения / Software Engineering",
    description:
      "Для этого изучаются языки (C, C++, C#, Java, JavaScript) и технологии программирования, в том числе технологии разработки web-приложений, приобретаются умения проектирования распределенных систем хранения и аналитики данных, формируются навыки администрирования серверных операционных систем (как семейства GNU/Linux, так и семейства Windows Server).",
    bacgroundImages: [Education_image_2, Education_image_1],
  },
];
