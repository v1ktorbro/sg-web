interface I {
  id: number;
  name: string;
  title: string;
}

export const DATA_TEAM: I[] = [
  {
    id: 1,
    name: "Павел Кузнецов",
    title: "Парикмахер",
  },
  {
    id: 2,
    name: "Ангелина Андреева",
    title: "Няня",
  },
  {
    id: 3,
    name: "Маргарита Лазарева",
    title: "Шахматист",
  },
  {
    id: 4,
    name: "Кирилл Соколов",
    title: "Пекарь",
  },
  {
    id: 5,
    name: "Виктория Сорокина",
    title: "Хирург",
  },
  {
    id: 6,
    name: "Софья Фомина",
    title: "Продакт-менеджер",
  },
  {
    id: 7,
    name: "Никита Прокофьев",
    title: "Монтажник",
  },
  {
    id: 8,
    name: "Максим Корнев",
    title: "Бренд-дизайнер",
  },
];
