import {
  IHoverNavLink,
  INavLink,
  ItemsOnHoverIdsEnum,
  SectionsIdsEnum,
} from "../types/header";

export const DATA_HEADER_MENU_LIST: INavLink[] = [
  { name: "Главная", id: SectionsIdsEnum.INTRO },
  { name: "О нас", id: SectionsIdsEnum.ABOUT },
  { name: "Cilab", id: SectionsIdsEnum.CILAB },
  { name: "Проекты", id: SectionsIdsEnum.PROJECT },
  { name: "Образование", id: SectionsIdsEnum.EDUCATION },
  { name: "Публикации", id: SectionsIdsEnum.PUBLICATIONS },
  { name: "Команда", id: SectionsIdsEnum.TEAM },
  { name: "Контакты", id: SectionsIdsEnum.CONTACTS },
];

export const DATA_HEADER_HOVER_ITEM_LIST: IHoverNavLink[] = [
  { name: "Аспирантура", id: ItemsOnHoverIdsEnum.PHDSTUDENT },
  { name: "Магистратура", id: ItemsOnHoverIdsEnum.MAGISTRACY },
];
