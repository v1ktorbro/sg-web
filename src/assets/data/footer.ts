import {
  IFooterAboutLink,
  IFooterDevelopmentLink,
  IFooterNewsCard,
} from "../types/footer";
import { ItemsOnHoverIdsEnum, SectionsIdsEnum } from "../types/header";

import Footer_card_image_1 from "../images/journals/epi.png";
import Footer_card_image_2 from "../images/journals/razrabotki.png";
import Footer_card_image_3 from "../images/journals/vestnik.png";

export const DATA_FOOTER_LIST_ABOUT: IFooterAboutLink[] = [
  { name: "О нас", id: SectionsIdsEnum.ABOUT },
  { name: "Главная", id: SectionsIdsEnum.INTRO },
  { name: "Команда", id: SectionsIdsEnum.TEAM },
  { name: "CILab", id: SectionsIdsEnum.CILAB },
  { name: "Контакты", id: SectionsIdsEnum.CONTACTS },
];

export const DATA_FOOTER_LIST_DEVELOPMENT: IFooterDevelopmentLink[] = [
  { name: "Образование", id: SectionsIdsEnum.EDUCATION },
  { name: "Аспирантура", id: ItemsOnHoverIdsEnum.PHDSTUDENT },
  { name: "Магистратура", id: ItemsOnHoverIdsEnum.MAGISTRACY },
  { name: "Публикации", id: SectionsIdsEnum.PUBLICATIONS },
  { name: "Проекты", id: SectionsIdsEnum.PROJECT },
];

export const DATA_FOOTER_LIST_NEWS: IFooterNewsCard[] = [
  {
    id: 1,
    title:
      "Журнал «Наносистемы: физика, химия, математика». Журнал «Наносистемы: физика, химия, математика»",
    date: parseFloat("12.06.2020"),
    backgroundImage: Footer_card_image_1,
  },
  {
    id: 2,
    title: "Журнал «Научно-технический вестник инф.тех, мех. и оптики»",
    date: parseFloat("11.08.2020"),
    backgroundImage: Footer_card_image_2,
  },
  {
    id: 3,
    title: "Последние новости Научные разработки",
    date: parseFloat("25.05.2020"),
    backgroundImage: Footer_card_image_3,
  },
];
